import os
import uuid

from flask import Flask, redirect, request
import requests

SECURITY_KEY = 'RYCFC6JZ49'
DEPLOYED_URL = os.getenv('DEPLOYED_URL')
app = Flask(__name__)
transactions = {}
INFO = 'INFO'


def log(msg, level=INFO):
    print(f'{level}: {msg}', flush=True)


def generate_trx_id():
    return ''.join(['{', str(uuid.uuid4()).upper(), '}'])


@app.route('/ping/', methods=['GET'])
def ping():
    return 'pong'


@app.route('/', methods=['POST'])
def register_trx():
    log(f'Registered trx {request}')
    vps_trx_id = generate_trx_id()
    transactions[vps_trx_id] = {
        'NotificationURL': request.form['NotificationURL'],
        'VendorTxCode': request.form['VendorTxCode']
    }
    return f'VPSProtocol=3.00\nStatus=OK\nStatusDetail=2014 : The Transaction was Registered Successfully\n' \
           f'VPSTxId={vps_trx_id}\nSecurityKey={SECURITY_KEY}\nNextURL={DEPLOYED_URL}/pay' \
           f'?vpstxid={vps_trx_id}'


@app.route('/pay', methods=['GET'])
def pay():
    log(f'Pay: {request}')
    trx = transactions.get(request.args.get('vpstxid', ''), None)
    if trx:
        log('Found trx, sending data to notification URL...')
        data = {
            'VendorTxCode': trx['VendorTxCode'],
            'VPSTxId': request.args.get('vpstxid'),
            'TxType': 'PAYMENT',
            'Status': 'OK',
            'StatusDetail': '0000 : The Authorisation was Successful.',
            'TxAuthNo': '869181503',
            'AVSCV2': 'SECURITY CODE MATCH ONLY',
            'AddressResult': 'NOTMATCHED',
            'PostCodeResult': 'MATCHED',
            'CV2Result': 'MATCHED',
            'GiftAid': '0',
            '3DSecureStatus': 'OK',
            'CAVV': 'kBMUUe+CSgX+jVymAUC/z4Jhr55D',
            'CardType': 'MC',
            'Last4Digits': '9634',
            'VPSSignature': 'FBFEDC4CD1AAF029BEC72ED7B680F1B2',
            'ExpiryDate': '0824',
            'BankAuthCode': '823018',
            'Decline`code': '00'
        }
        response = requests.post(trx['NotificationURL'], data)
        body = response.content.decode('utf-8')
        log(f'Got response body {response}')
        redirect_url = body.split('\n')[1].split('=')[1]
        return redirect(redirect_url)
    else:
        return 'Transaction not found', 404
